#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER_ID=${_UID:-9001}

echo "Starting with UID : $USER_ID"
useradd --shell /bin/bash -u $USER_ID -o -c "" -m dev
export HOME=/home/dev

if [ -f /mnt/host/.gitconfig ];then
    cp -a /mnt/host/.gitconfig /home/dev/.gitconfig
    chown dev:dev /home/dev/.gitconfig
fi

if [ -d /mnt/host/.ssh ];then
    cp -a /mnt/host/.ssh /home/dev/.ssh

    chown -R dev:dev /home/dev/.ssh 
    chmod 600 /home/dev/.ssh/*

    echo "ssh-agent -s > /tmp/ssh.agent ; . /tmp/ssh.agent > /dev/null" >> /home/dev/.bashrc
fi

mkdir /logs
chown dev:dev /logs

#exec /usr/local/bin/gosu dev "$@"
exec "$@"
