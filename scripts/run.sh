#!/bin/env bash

rm -rf /run/httpd/* /tmp/httpd*

ln -sfn /logs /etc/httpd/logs

sed -i "s|/src/html|/src/${HTTPD_DOCROOT}|g" /etc/httpd/conf/httpd.conf
sed -i "s/php_value session.save_path/#php_value session.save_path/g" /etc/httpd/conf.modules.d/10-php.conf
sed -i "s/php_value soap.wsdl_cache_dir/#php_value soap.wsdl_cache_dir/g" /etc/httpd/conf.modules.d/10-php.conf

PHP_MEMORY_LIMIT=${PHP_MEMORY_LIMIT:-8096M}
sed -i "s#\(memory_limit = \).*#\1${PHP_MEMORY_LIMIT}#g" /etc/php.d/php-default.ini

# detect xdebug configuration file
xdebug_file=$(php --ini | grep -F 'xdebug.ini' | tr -d ',')

# enable if wanted
PHP_XDEBUG=${PHP_XDEBUG:-0}
if [ ${PHP_XDEBUG} -eq 1 ]; then
  sed -i 's/^;\(zend_extension\)/\1/' ${xdebug_file}
else
  # or disable
  sed -i 's/^\(zend_extension\)/;\1/' ${xdebug_file}
fi

bash /php_xdebug_composer.sh >> ${HOME}/.bashrc

supervisord -n
