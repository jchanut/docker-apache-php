# apache-php  

Docker image for Apache and PHP.

## Includes

- Apache/2.4.6
- Composer version 1.4.0 (prestaconcept)
- Git 1.8
- Node v6.x (nodesource release)
- Php 7.1.7 (prestaconcept)
- Make 3.82
- Wkhtmltopdf 0.12.3 (prestaconcept)

### PHP tools

Web access
- direct acces : http://<container-ip>:8080
- from docker-compose port mapping : http://localhost:1081

- Opcache Dashboard <https://github.com/carlosbuenosvinos/opcache-dashboard/>

### PECL

- APCu
- Imagick
- pecl_HTTP
- lz4
- mongodb
- ssh2
- redis
- AST
- XDebug (To enable xdebug, add environement variable PHP_XDEBUG setted to 1)

## Usage

Apache docroot is /src/web. For symfony, you have to mount volume on /src and have your web resources in web. You can
configure docroot setting the environment variable *HTTPD_DOCROOT*.

Apache runs with user "dev". The user is created at boot in the entrypoint script with the uid from env variable _UID.

Apache listens on 80 (http).

Log files are stored in /logs.

## docker-compose.yml

### php configuration

You can override default php memory_limit with *PHP_MEMORY_LIMIT* variable

default value is 8096M
