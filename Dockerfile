FROM centos:7

MAINTAINER David Gaussinel <dgaussinel@prestaconcept.net>

RUN yum install -y yum-utils \
    && yum install -y http://rpm.prestaconcept.net/prestaconcept/RHEL7/presta-release-7-3.el7.centos.noarch.rpm \
    && yum-config-manager --enable prestaconcept \
    && yum-config-manager --enable prestaconcept-php71

RUN yum clean metadata

# Remove root password
RUN usermod -p "" root

# Install nodejs repository
RUN curl -sL https://rpm.nodesource.com/setup_6.x | bash -
RUN yum -y install nodejs --disablerepo=epel

# Install packages
RUN yum -y install \
      wkhtmltox-bundle composer \
      git \
      httpd \
      make \
      which

RUN yum -y install \
    php \
    php-bcmath \
    php-cli \
    php-curl \
    php-dba \
    php-dbg \
    php-enchant \
    php-gd \
    php-gettext \
    php-gmp \
    php-imap \
    php-intl \
    php-ldap \
    php-litespeed \
    php-mbstring \
    php-mcrypt \
    php-mysql \
    php-odbc \
    php-opcache \
    php-openssl \
    php-process \
    php-pdo \
    php-pdo_sqlite \
    php-pdo_dblib \
    php-pgsql \
    php-pspell \
    php-recode \
    php-snmp \
    php-soap \
    php-tidy \
    php-xml \
    php-xmlrpc \
    php-zip

# for now install fail with epel repo enabled
RUN yum -y install --disablerepo=epel \
    php-pecl-apcu \
    php-pecl-imagick \
    php-pecl-http \
    php-pecl-lz4 \
    php-pecl-mongodb \
    php-pecl-ssh2 \
    php-pecl-redis \
    php-pecl-xdebug \
    php-pecl-ast


RUN ln -sf /usr/share/zoneinfo/Europe/Paris  /etc/localtime \
    && chmod -R og+r /var/log/httpd \
    && usermod -p "" root \
    && sed -i  "s/#\(ServerName\).*/\1 ${HOSTNAME}/" /etc/httpd/conf/httpd.conf \
    && sed -i "s/User apache/User dev/" /etc/httpd/conf/httpd.conf \
    && sed -i "s/Group apache/Group dev/" /etc/httpd/conf/httpd.conf \
    && sed -i "s/AllowOverride None/AllowOverride All/g" /etc/httpd/conf/httpd.conf \
    && sed -i "s/var\/www/src/g" /etc/httpd/conf/httpd.conf \
    && sed -i '/CustomLog/d' /etc/httpd/conf/httpd.conf

RUN yum install -y bash-completion

RUN curl https://dl.yarnpkg.com/rpm/yarn.repo > /etc/yum.repos.d/yarn.repo \
    && yum -y install yarn

RUN curl -LsS https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o /usr/bin/wp \
    && chmod +x /usr/bin/wp


# to manage additional running services
RUN yum install -y supervisor

ADD conf/supervisor/supervisord.conf /etc/supervisord.conf
ADD conf/supervisor/*.ini /etc/supervisord.d/

RUN rm -rf /run/httpd/* /tmp/httpd* \
    && yum clean all

RUN groupdel games

ADD scripts/entry.sh /entry.sh
ENTRYPOINT ["sh", "/entry.sh"]

ADD scripts/run.sh /run.sh

ADD tools /var/www/html/tools

RUN rm /etc/httpd/conf.d/welcome.conf

ADD scripts/php_xdebug_composer.sh /php_xdebug_composer.sh

ADD conf/php/php-default.ini /etc/php.d/php-default.ini

ADD conf/httpd/*.conf /etc/httpd/conf.d/

ADD conf/php/xdebug-config.ini /etc/php.d/

ADD conf/os/ps1.sh /etc/profile.d/ps1.sh

# Add CA
RUN curl http://key.presta.in/led.pem -o /etc/pki/ca-trust/source/anchors/led.pem
RUN /bin/update-ca-trust

EXPOSE 80
EXPOSE 8080

ENV HTTPD_DOCROOT web

CMD ["bash", "/run.sh"]
