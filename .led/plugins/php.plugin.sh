_php_plugin_desc()
{
    echo "Manage php (launch php, switch xdebug status)"
}

# plugin

# Plugin usage:
#   led php [options] [COMMAND]
#   led php -h|--help
#
# Commands :
#   xdebug          Manage XDebug status
#   memory          Set php memory_limit value
#
# @autocomplete php: xdebug memory
php_plugin()
{
  local command=$1
  local options=$@

  [[ $command == -* ]] && command=""
  options=( "${options[@]/$command}" )


  # ensure constant for jyparser is available
  if [ -z "${VENDORS_BIN_JYPARSER}" ]; then
    echo "This plugin cannot work with this version of led, please upgrade"
    exit 1
  fi

  case $command in
      xdebug) php_xdebug $options;;
      memory) php_memory $options;;
      "") help plugin php;;
      *) echo -e "Unknown plugin command : $command\n"; help plugin php;;
  esac
}

# xdebug
# Usage: led php xdebug [OPTIONS]
#
# Manage Xdebug status in a docker-compose file
#
# Options :
#  -e, --enable   Enable XDebug
#  -d, --disable  Disable XDebug
#
# Without option, switch XDebug status
# Each operation refresh the container
#
# To override compose file, see led README
#
# @autocomplete php xdebug: --enable --disable
php_xdebug()
{
    local xdebug_operation="switch"
    local ret=
    local message=

    if ! func_exists _dockercompose_file_check 2>/dev/null; then
      echo "please upgrade LED" >&2
      exit 1
    fi
    _dockercompose_file_check

    local dc_file="${LED_DOCKERCOMPOSE_FILE}"
    local dc_key=".services.apache.environment.PHP_XDEBUG"
    local dc_file_tmp=${dc_file}.tmp

    _GETOPT_SHORT="de"
    _GETOPT_LONG_ARRAY=(
        "disable"
        "enable"
    )

    set -- $(_getopt "$@")

    while [ ! -z "$#" ]; do
      case $1 in
          -e|--enable) xdebug_operation="enable"; shift;;
          -d|--disable) xdebug_operation="disable"; shift;;
          --) shift;break;;
      esac
    done

    if [ ! -f "${dc_file}" ]; then
      echo >&2 "${dc_file} not found"
      exit 1
    fi

    local xdebug_value=

    case ${xdebug_operation} in
        enable) xdebug_value=1;;
        disable) xdebug_value=0;;
        switch)
          xdebug_value=$(${VENDORS_BIN_JYPARSER} "${dc_file}" get "${dc_key}")
          [ "${xdebug_value}" == "null" ] && xdebug_value=0
          # value can be 1 or 0, so substract to reverse
          xdebug_value=$((1 - xdebug_value))
          ;;
    esac


    if [ ${xdebug_value} -eq 0 ]; then
      ${VENDORS_BIN_JYPARSER} "${dc_file}" del "${dc_key}" > ${dc_file_tmp} \
      && mv "${dc_file_tmp}" "${dc_file}"
      ret=$?
      message="XDebug disabled"
    else
      ${VENDORS_BIN_JYPARSER} "${dc_file}" set "${dc_key}" 1 > ${dc_file_tmp} \
      && mv "${dc_file_tmp}" "${dc_file}"
      ret=$?
      message="XDebug enabled"
    fi


    # refresh container
    if [ $ret -eq 0 ]; then
      echo ${message}
      $0 up
    fi


}

# memory
# Usage: led php memory [memory_in_MB]
#
# Manage PHP memory_limit value a docker-compose file
#
# Without value, remove entry to get container default value
# Each operation refresh the container
#
# To override compose file, see led README
#
# @autocomplete php memory:
php_memory()
{

  if ! func_exists _dockercompose_file_check 2>/dev/null; then
    echo "please upgrade LED" >&2
    exit 1
  fi
  _dockercompose_file_check

  local dc_file="${LED_DOCKERCOMPOSE_FILE}"
  local dc_file_tmp=${dc_file}.tmp
  local dc_key=".services.apache.environment.PHP_MEMORY_LIMIT"

  local message=
  local ret=

  if [ ! -f "${dc_file}" ]; then
    echo >&2 "${dc_file} not found"
    exit 1
  fi

  local memory=$1

  if [ -z "$memory" ]; then
    # if no memory set, remove entry from configuration to get default container value
    ${VENDORS_BIN_JYPARSER} "${dc_file}" del "${dc_key}" > ${dc_file_tmp} \
    && mv "${dc_file_tmp}" "${dc_file}"
    ret=$?
    message="Memory limit is restored to the default value"
  else
    # if memory is set, override default container value
    if [[ ! $memory =~ ^[0-9]+$ ]]; then
      echo >&2 "memory value should be a number"
      exit 1
    fi

    # set memory value in megabyte
    memory=${memory}M

    ${VENDORS_BIN_JYPARSER} "${dc_file}" set "${dc_key}" \"${memory}\" > ${dc_file_tmp} \
    && mv "${dc_file_tmp}" "${dc_file}"
    message="Memory limit fixed to ${memory}"
    ret=$?
  fi

  # refresh container
  if [ $ret -eq 0 ]; then
    echo ${message}
    $0 up
  fi

}
