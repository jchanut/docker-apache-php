#!/bin/bash

APACHE_SERVER="apache"

_apache_plugin_desc()
{
    echo "Manage apache-php image (connect, view logs, clear apc)"
}

# access
# Usage: led apache access
#
# View apache access log
# Press CTRL+C to exit log
apache_access()
{
    ssh "--server" $APACHE_SERVER "--user" "dev" "tailf /logs/access_log"
}

# error
# Usage: led apache error
#
# View apache error log
# Press CTRL+C to exit log
apache_error()
{
    ssh "--server" $APACHE_SERVER "--user" "dev" "tailf /logs/error_log"
}

# clear
# Usage: led apache clear
#
# Clear OPC Cache
apache_clear()
{
    local target=$(_docker_find_container_name "${APACHE_SERVER}")
    local env="HTTPD_DOCROOT"
    local docroot=$(docker inspect -f '{{range $_, $e := .Config.Env}}{{println $e}}{{end}}' $target|grep $env|sed s/${env}=//g)

    local file=$(mktemp --tmpdir=./$docroot --suffix='.php')
    echo '<?php opcache_reset(); echo "Opc Cache cleared\n"; ?>' > $file
    ssh "--server" $APACHE_SERVER "--user" "dev" "curl http://127.0.0.1/$(basename $file)"
    rm $file
}

# docroot
# Usage: led apache docroot <directory>
#
# Set HTTPD_DOCROOT value in a docker-compose file
#
# the directory should be relative to the current path
#
# To override compose file, see led README
apache_docroot()
{
  if ! func_exists _dockercompose_file_check 2>/dev/null; then
    echo "please upgrade LED" >&2
    exit 1
  fi
  _dockercompose_file_check

  local dc_file="${LED_DOCKERCOMPOSE_FILE}"

  local dc_file_tmp="${dc_file}.tmp"
  local dc_key=".services.apache.environment.HTTPD_DOCROOT"

  local message=
  local ret=

  if [ ! -f "${dc_file}" ]; then
    echo >&2 "${dc_file} not found"
    exit 1
  fi

  local httpd_docroot=$1

  if [ -z "${httpd_docroot}" ]; then
    echo "directory should be set"
    exit 1
  else

    if [ ! -d "${PWD}/${httpd_docroot}" ]; then
      echo "the directory ${PWD}/${httpd_docroot} should exists"
      exit 1
    fi

    ${VENDORS_BIN_JYPARSER} "${dc_file}" set "${dc_key}" \"${httpd_docroot}\" > ${dc_file_tmp} \
    && mv "${dc_file_tmp}" "${dc_file}"
    message="HTTPD_DOCROOT fixed to '${httpd_docroot}'"
    ret=$?
  fi

  # refresh container
  if [ $ret -eq 0 ]; then
    echo ${message}
    $0 up
  fi
}

# plugin

# Plugin usage:
#   led apache [options] [COMMAND]
#   led apache -h|--help

#   -h, --help      Print usage
#   -s, --server    Apache server to use (default: apache)

# Commands :
#   access          Display access log
#   clear           Clear OPC Cache
#   docroot         Set project's document root
#   error           Display error log

# If no command is provided, connect to the apache server.
# Option --server can be used to change selected server
#
# @autocomplete apache: --server access clear error docroot
apache_plugin()
{
    command=$1
    if [[ $command == -* ]]; then command=""; fi
    options=$@
    options=( "${options[@]/$command}" )

    _GETOPT_SHORT="s:"
    _GETOPT_LONG_ARRAY=(
        "server:"
    )
    set -- $(_getopt "$@")

    local help=0
    while [ ! -z "$#" ]; do
    case $1 in
        -s|--server) APACHE_SERVER=$2; shift 2;;
        --) shift;break;;
        -*) echo "bad option:$1"; shift; break;;
    esac
    done

    case $command in
        access) apache_access;;
        clear) apache_clear;;
        error) apache_error;;
        docroot) apache_docroot ${options};;
        "") ssh "--server" $APACHE_SERVER "--user" "dev";;
        *) echo -e "Unknown plugin command : $command\n"; help plugin apache;;
    esac
}
